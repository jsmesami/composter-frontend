(ns mkp.composter.preload
  (:require
    [mkp.composter.utils.debug :refer [log]]))


(set! (.-log js/window) log)

(enable-console-print!)
