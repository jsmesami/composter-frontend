(ns mkp.composter.core
  (:require
    [reagent.core :as reagent]
    [re-frame.core :as reframe]
    [mkp.composter.alert.core]
    [mkp.composter.app.core]
    [mkp.composter.app.views :refer [app]]
    [mkp.composter.generator.core]
    [mkp.composter.modals.core]
    [mkp.composter.net.core]
    [mkp.composter.posters.core]
    [mkp.composter.resources.core]
    [mkp.composter.views.core]))


(defn render!
  []
  (reagent/render
    [app]
    (js/document.getElementById "app")))


(defn ^:export main
  []
  (reframe/dispatch-sync [:app/initialize])
  (render!))


(defn on-js-reload
  []
  (render!))
