(ns mkp.composter.views.edit
  (:require
    [mkp.composter.generator.views.core :refer [generator]]
    [mkp.composter.components.footer :refer [footer]]
    [mkp.composter.components.navbar :refer [navbar]]))


(defn edit
  []
  [:div#edit.view
   [navbar
    [:h1 "Generátor"]]
   [generator]
   [footer]])
