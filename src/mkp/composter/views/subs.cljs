(ns mkp.composter.views.subs
  (:require
    [re-frame.core :refer [reg-sub]]
    [mkp.composter.views.home :refer [home]]
    [mkp.composter.views.edit :refer [edit]]))


(def view-id->view
  {:home home
   :edit edit})


(reg-sub
  :views/current
  (fn [db _]
    (-> db :view view-id->view)))
