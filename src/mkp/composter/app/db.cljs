(ns mkp.composter.app.db
  (:require
    [mkp.composter.posters.db :refer [PosterListInitial]]
    [mkp.composter.net.db :refer [NetInitial]]))


(def AppInitial
  {:net  NetInitial
   :posters PosterListInitial
   :view nil
   :modal nil})


(def app-routes
  ["/" {"" :home
        "generator/" :edit}])
