(ns mkp.composter.app.views
  (:require
    [re-frame.core :refer [subscribe]]
    [mkp.composter.alert.views :refer [alerts]]
    [mkp.composter.components.loader :refer [loader]]))


(defn app
  []
  (let [view @(subscribe [:views/current])
        modal @(subscribe [:modals/current])]
    [:div
     [loader]
     [alerts]
     (when view
       [view])
     (when modal
       [modal])]))
