(ns mkp.composter.app.events
  (:require
    [re-frame.core :refer [reg-event-fx]]
    [mkp.composter.app.db :refer [AppInitial]]))


(reg-event-fx
  :app/initialize
  (fn [_]
    {:db AppInitial
     :dispatch [:resources/fetch-initial-data]
     :app/init-history nil}))
