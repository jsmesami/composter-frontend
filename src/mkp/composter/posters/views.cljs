(ns mkp.composter.posters.views
  (:require
    [re-frame.core :refer [subscribe]]
    [mkp.composter.posters.components.filter :refer [poster-filter]]
    [mkp.composter.posters.components.pagination :refer [pagination]]
    [mkp.composter.posters.components.thumbs :refer [poster-thumbs]]))


(defn poster-list
  []
  (let [posters @(subscribe [:posters/list])]
    [:div.container
     [poster-filter]
     [:div.poster-list
      [poster-thumbs posters]
      [pagination posters]]]))
