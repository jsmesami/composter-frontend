(ns mkp.composter.posters.events
  (:require
    [clojure.string :as s]
    [reagent.format :refer [format]]
    [re-frame.core :refer [reg-event-fx trim-v]]
    [mkp.composter.alert.core :refer [status->kind]]
    [mkp.composter.posters.db :refer [PosterFilterInitial]]
    [mkp.composter.resources.core :refer [poster-resource]]
    [mkp.composter.utils.url :refer [m->qs]]))


(reg-event-fx
  :posters/reload
  (fn [{:keys [db]}]
    (let [posters (:posters db)
          resource (get-in db [:resources :endpoints :poster])]
      (when resource
        {:dispatch [:net/json-xhr :get (str resource (m->qs (:filter posters)))
                    :success-fx (fn [db response]
                                  {:db (-> db
                                           (assoc-in [:posters :count] (:count response))
                                           (assoc-in [:posters :next?] (string? (:next response)))
                                           (assoc-in [:posters :prev?] (string? (:previous response)))
                                           (assoc-in [:posters :list] (:results response)))})]}))))


(reg-event-fx
  :posters/update-filter
  [trim-v]
  (fn [{:keys [db]} [f]]
    {:db (update-in db [:posters :filter] #(merge % f))
     :dispatch [:posters/reload]}))


(reg-event-fx
  :posters/reset-filter
  [trim-v]
  (fn [{:keys [db]}]
    {:db (assoc-in db [:posters :filter] PosterFilterInitial)
     :dispatch [:posters/reload]}))


(defn poster-clone-success
  [db]
  {:db db
   :dispatch-n [[:alert/add-message "Leták byl duplikován." :success :timeout 8000]
                [:posters/reload]]})


(reg-event-fx
  :posters/clone
  [trim-v]
  (fn [{:keys [db]} [poster]]
    (when-let [resource (get-in db [:resources :endpoints :poster-clone])]
      {:dispatch [:net/json-xhr :post (s/replace resource #"\<pk\>" (str (:id poster)))
                  :success-fx poster-clone-success]})))


(defn poster-delete-success
  [db]
  {:db db
   :dispatch-n [[:alert/add-message "Leták byl úspěšně smazán." :success :timeout 8000]
                [:posters/reload]]})


(defn poster-delete-failure
  [db response]
  (let [failure-kind (-> response :status status->kind)]
    (-> {:db db}
        (merge
          (if (= :server-error failure-kind)
            {:dispatch [:alert/add-message "Spojení se nezdařilo." failure-kind]}
            {:dispatch-n [[:alert/add-message "Leták se nepodařilo smazat." failure-kind]
                          [:posters/reload]]})))))


(reg-event-fx
  :posters/delete
  [trim-v]
  (fn [{:keys [db]} [poster]]
    (when (js/confirm (format "Nevratná operace.\nOpravdu chcete smazat leták \"%s\"?" (:title poster)))
      {:dispatch [:net/json-xhr :delete (poster-resource db (:id poster))
                  :success-fx poster-delete-success
                  :failure-fx poster-delete-failure]})))


(reg-event-fx
  :posters/preview
  [trim-v]
  (fn [_ [link]]
    {:dispatch [:modals/set :preview-poster {:thumb link}]}))


(reg-event-fx
  :posters/edit
  [trim-v]
  (fn [_ [poster]]
    (let [fx {:dispatch [:generator/prepare poster]}
          poster-age-ms (- (js/Date.) (js/Date. (:modified poster)))
          three-days-ms (* 3 24 60 60 1000)]
      (if (> poster-age-ms three-days-ms)
        (when (js/confirm "Leták je starší než 3 dny.\nOpravdu ho chcete editovat?")
          fx)
        fx))))


(reg-event-fx
  :posters/create
  [trim-v]
  (fn [_]
    {:dispatch [:generator/prepare]}))
