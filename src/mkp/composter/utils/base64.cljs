(ns mkp.composter.utils.base64)


(defn file->base64
  [file callback]
  (let [reader (js/FileReader.)]
    (set! (.-onload reader) #(callback (-> % .-target .-result)))
    (.readAsDataURL reader file)))


(defn url->base64
  [url callback]
  (-> (js/fetch url)
      (.then #(.blob %))
      (.then #(file->base64 % callback))))
