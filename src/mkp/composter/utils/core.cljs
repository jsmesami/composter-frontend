(ns mkp.composter.utils.core)


(defn inc-loading-count
  [db]
  (update-in db [:net :loading-count] inc))


(defn dec-loading-count
  [db]
  (update-in db [:net :loading-count] #(if (pos? %) (dec %) 0)))
