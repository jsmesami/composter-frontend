(ns mkp.composter.components.navbar
  (:require
    [mkp.composter.utils.bem :as bem]))


(def module-name "navbar")


(defn navbar
  [& children]
  [:div.row {:class module-name}
   (->> children
        (into [:div.col-sm-7
               {:class (bem/be module-name "children")}]))])
