(ns mkp.composter.modals.subs
  (:require
    [re-frame.core :refer [reg-sub]]
    [mkp.composter.modals.views.spec-modal :refer [select-spec]]
    [mkp.composter.modals.views.preview-modal :refer [preview-poster]]))


(def modal-id->modal
  {:select-spec select-spec
   :preview-poster preview-poster})


(reg-sub
  :modals/current
  (fn [db _]
    (modal-id->modal (get-in db [:modal :id]))))


(reg-sub
  :modals/data
  (fn [db _]
    (get-in db [:modal :data])))
