(ns mkp.composter.generator.views.dispatchers
  (:require
    [re-frame.core :refer [dispatch]]
    [mkp.composter.settings :refer [max-image-length]]
    [mkp.composter.utils.base64 :refer [file->base64 url->base64]]
    [mkp.composter.utils.string :refer [prepos]]
    [mkp.composter.utils.url :refer [get-filename]]))


(defn- update-field-dispatcher
  [field-id kvs]
  (doseq [[key value] kvs]
    (dispatch [:generator/update-form-field field-id key value])))


(defn- resize-image
  [img width height]
  (let [canvas (doto (js/document.createElement "canvas")
                 (-> .-width (set! width))
                 (-> .-height (set! height)))
        context (.getContext canvas "2d")]
    (.drawImage context img 0 0 width height)
    (-> context .-canvas (.toDataURL "image/jpeg" 0.65))))


(defn constrain-image-size
  [img]
  (let [width (.-width img)
        height (.-height img)]
    (if (> (max width height) max-image-length)
      (let [ratio (min (/ max-image-length height) (/ max-image-length width))]
        (resize-image img (* width ratio) (* height ratio)))
      (.-src img))))


(defn- store-image-data
  [field-id filename base64-data]
  (update-field-dispatcher field-id
    {:data base64-data
     :filename filename
     :error nil
     :changed true}))


(defn- prepare-image
  [field-id filename base64-data]
  (doto (js/Image.)
    (-> .-src (set! base64-data))
    (-> .-onload (set! #(->> (.-target %)
                             constrain-image-size
                             (store-image-data field-id filename))))))


(defmulti on-change-image-dispatcher
          (fn [_ data]
            (type data)))


(defmethod on-change-image-dispatcher js/File
  [field-id img-file]
  (file->base64 img-file (partial prepare-image field-id (.-name img-file))))


(defmethod on-change-image-dispatcher js/String
  [field-id img-url]
  (url->base64 img-url (partial prepare-image field-id (get-filename img-url))))


(defn on-change-text-dispatcher
  [field-id text]
  (update-field-dispatcher field-id {:text (prepos text)
                                     :changed true}))
