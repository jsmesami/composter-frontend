(ns mkp.composter.generator.views.fields
  (:require
    [re-frame.core :refer [dispatch subscribe]]
    [mkp.composter.components.basic :refer [icon input select select-image]]
    [mkp.composter.generator.form :refer [field-filled?]]
    [mkp.composter.generator.views.components :refer [help-text char-counter error-msg]]
    [mkp.composter.generator.views.dispatchers :refer [on-change-text-dispatcher on-change-image-dispatcher]]
    [mkp.composter.resources.core :refer [resource->options]]
    [mkp.composter.utils.string :refer [shorten]]))


(defmulti render-field
          (fn [_ [_ field]]
            (-> field :type keyword)))


(defmethod render-field :text
  [loading? [id field]]
  (let [{:keys [name text help_text error widget char_limit mandatory hidden]} field]
    [:label
     name
     (when mandatory (if (field-filled? field) [icon "check"] [icon "star"]))
     [help-text help_text]
     [input
      :value text
      :on-change #(on-change-text-dispatcher id %)
      :enabled? (not loading?)
      :classes ["form-control" (when error "is-invalid")]
      :widget (keyword (or widget :input))
      :attrs {:id id
              :maxLength char_limit
              :readOnly hidden}]
     [char-counter text char_limit]
     [error-msg error]]))


(defmethod render-field :image
  [loading? [id field]]
  (let [{:keys [name filename url data help_text error mandatory hidden]} field]
    [:div.file-selector
     [:label
      name
      (when mandatory (if (field-filled? field) [icon "check"] [icon "star"]))
      [:br]
      [help-text help_text]
      (when-not hidden
        [:div
         [:label.btn.btn-outline-primary.btn-sm
          {:for id}
          (shorten (or filename "vyberte obrázek (JPEG nebo PNG)") 32)]
         [select-image
          :on-change #(on-change-image-dispatcher id %)
          :enabled? (not loading?)
          :classes ["form-control-file" (when error "is-invalid")]
          :attrs {:id id
                  :accept "image/png, image/jpeg"}]])]
     [error-msg error]
     (when-let [uri (or data url)]
       [:div.generator__thumb
        [:img.img-thumbnail {:src uri}]])]))


(defn bureau-special-field
  "Special field (not present in form fields). Adds bureau ID to the form
  and also populates read-only but mandatory 'lib_name', 'lib_address' and 'lib_logo' form fields."
  [loading? form]
  (let [bureau-resource @(subscribe [:resources/bureau])
        bureaus-by-id (reduce #(assoc %1 (:id %2) %2) {} bureau-resource)]
    [:div.row.justify-content-around
     [:div.form-group.col-md-8
      [:label
       "Knihovna" (if (pos? (:bureau form)) [icon "check"] [icon "star"])
       [select (resource->options bureau-resource)
        :value (:bureau form)
        :classes ["form-control"]
        :enabled? (not loading?)
        :on-change (fn [id] (let [bureau-id (int id)]
                              (dispatch [:generator/reset-form (assoc form :bureau bureau-id)])
                              (on-change-text-dispatcher :lib_name (-> bureau-id bureaus-by-id :name))
                              (on-change-text-dispatcher :lib_address (-> bureau-id bureaus-by-id :address))
                              (on-change-image-dispatcher :lib_logo (-> bureau-id bureaus-by-id :logo))))]]]]))


(defn form-fields
  [loading? form]
  [:div
   [bureau-special-field loading? form]
   (for [[id field] (sort-by #(-> % second :order) (:fields form))]
     ^{:key id}
     [:div.row.justify-content-around
      [:div.form-group.col-md-8
       (render-field loading? [id field])]])])
