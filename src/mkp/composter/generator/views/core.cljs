(ns mkp.composter.generator.views.core
  (:require
    [re-frame.core :refer [subscribe]]
    [mkp.composter.generator.views.buttons :refer [generator-buttons]]
    [mkp.composter.generator.views.fields :refer [form-fields]]))


(defn generator
  []
  (let [form @(subscribe [:generator/form])
        loading? @(subscribe [:net/loading?])]
    [:div.generator.container
     [form-fields loading? form]
     [generator-buttons loading? form]]))
