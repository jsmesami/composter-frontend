(ns mkp.composter.settings)


(goog-define ^:string api-server "http://127.0.0.1:8000")


(goog-define ^:string api-prefix "")


(def default-request-timeout 20000)


(def max-image-length 1600)
